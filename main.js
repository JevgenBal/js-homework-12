let buttons = document.querySelectorAll(".btn");
let activeButton = null;

function activateButton(button) {
  if (activeButton !== null) {
    activeButton.classList.remove("active");
  }
  button.classList.add("active");
  activeButton = button;
}

buttons.forEach((button) => {
  button.addEventListener("click", () => {
    activateButton(button);
  });
});

document.addEventListener("keydown", (event) => {
  let key = event.key.toUpperCase();
  let button = document.querySelector(`[data-key="${key}"]`);
  if(button){
    activateButton(button);
  }
});
